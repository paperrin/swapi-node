const MyError = require.main.require('./MyError');

module.exports = {
    method: 'GET',
    path: '/{any*}',
    handler: async (_, h) => {
        const error = new MyError('NOT_FOUND', 404, 'This resource isn\'t available');
        return h.response(error.toJSON()).code(404);
    },
};