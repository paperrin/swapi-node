const MyError = require.main.require('./MyError');
const swData = require.main.require('./swData');

module.exports = {
    method: 'GET',
    path: '/api',
    handler: async req => {
        const originUrl = req.url.origin;
        let categoryNames;

        try {
            categoryNames = await swData.getCategoryNames();
        } catch(err) {
            return MyError.from(err).toJSON();
        }

        let res = {};
        categoryNames.forEach(categoryName => {
            res[categoryName] = `${originUrl}/${categoryName}`;
        });

        return res;
    },
};