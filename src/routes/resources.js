const MyError = require.main.require('./MyError');
const swData = require.main.require('./swData');
const Hoek = require('@hapi/hoek');

module.exports = {
    method: 'GET',
    path: '/api/{category}/{id?}',
    handler: async req => {
        const category = Hoek.escapeHtml(req.params.category);
        const id = Hoek.escapeHtml(req.params.id);
        const search = Hoek.escapeHtml(req.query.search);

        try {
            if (id.length)
                return await swData.getCategoryItemById(category, id);
            if (search)
                return await swData.searchInCategory(category, search);
            return await swData.getCategory(category);
        } catch (err) {
            return MyError.from(err).toJSON();
        }
    },
};