class MyError extends Error {
    constructor(code = 'GENERIC', status = 500, ...params) {
        super(...params);

        if (Error.captureStackTrace)
            Error.captureStackTrace(this, MyError);

        this.code = code;
        this.status = status;
    }

    toJSON() {
        return {
            error: {
                code: this.code,
                message: this.message,
                status: this.status,
                stackTrace: this.stack,
            }
        };
    }
}

MyError.from = function(err) {
    if (err instanceof MyError)
        return err;
    const newError = new MyError(undefined, undefined, err.message);
    newError.stack = err.stack;
    return newError;
};

module.exports = MyError;