const fsp = require('fs').promises;
const path = require('path');
const appDir = path.dirname(require.main.filename);
const resourcesDir = `${appDir}/../resources`;

let resources = {};

const swData = module.exports = {
    loadAll: async function() {
        const resourceFiles = await fsp.readdir(resourcesDir);
        for (const file of resourceFiles) {
            const [ category ] = file.split('.');
            const fileContent = await fsp.readFile(resourcesDir + '/' + file);
            resources[category] = JSON.parse(fileContent).map((value, index) => {
                value = value.fields;
                value.id = index + 1;
                value.category = category;
                return value;
            });
        }
    },
    getCategoryNames: async function() {
        return Object.keys(resources);
    },
    getCategory: async function(categoryName) {
        return resources[categoryName]
    },
    getCategoryItemById: async function(category, id) {
        const itemArray = await swData.getCategory(category);
        if (id > 0 && id <= itemArray.length)
            return itemArray[id - 1];
        return null;
    },
    searchInCategory: async function(category, search) {
        const itemArray = await swData.getCategory(category);
        const searchedFields = [ 'name', 'title' ];
        search = search.toLowerCase();

        return itemArray.filter(value => {
            let isResult = false;
            searchedFields.forEach(field => {
               const fieldValue = (value[field] || "").toLowerCase();
               if (fieldValue.includes(search))
                   isResult = true;
            });
            return isResult;
        });
    },
};
